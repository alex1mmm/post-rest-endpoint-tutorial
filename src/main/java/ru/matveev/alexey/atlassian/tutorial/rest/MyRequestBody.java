package ru.matveev.alexey.atlassian.tutorial.rest;

import lombok.Data;

@Data
public class MyRequestBody {
    String value;
}
