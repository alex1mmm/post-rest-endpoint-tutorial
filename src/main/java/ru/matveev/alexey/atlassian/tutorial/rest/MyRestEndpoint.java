package ru.matveev.alexey.atlassian.tutorial.rest;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * A resource of message.
 */
@Path("/message")
public class MyRestEndpoint {

    @POST
    @AnonymousAllowed
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response postMessage(MyRequestBody body)
    {
       return Response.ok(new MyRestEndpointModel(body.getValue())).build();
    }
}