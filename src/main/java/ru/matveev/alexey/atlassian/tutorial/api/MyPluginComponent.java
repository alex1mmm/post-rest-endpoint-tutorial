package ru.matveev.alexey.atlassian.tutorial.api;

public interface MyPluginComponent
{
    String getName();
}