package ru.matveev.alexey.atlassian.tutorial.rest;

import javax.xml.bind.annotation.*;
@XmlRootElement(name = "message")
@XmlAccessorType(XmlAccessType.FIELD)
public class MyRestEndpointModel {

    @XmlElement(name = "value")
    private String message;

    public MyRestEndpointModel() {
    }

    public MyRestEndpointModel(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}